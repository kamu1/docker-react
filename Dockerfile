FROM node:alpine as builder
WORKDIR '/home/node'

COPY --chown=node:node package.json .
RUN npm install

COPY --chown=node:node . /home/node

USER node
RUN npm run build

FROM nginx
EXPOSE 80
COPY --from=builder /home/node/build /usr/share/nginx/html
